/*jshint node: true, -W032, -W106*/
'use strict';

var http           = require('http');
var app            = require('./app/app.js');
var debug          = require('debug')('nestub:index');
// var Q              = require('q');
// Q.longStackSupport = true; //TODO not in production

var nestub = app();
//var path    = require('path');
//var express = require('express');
//nestub.use(express.static(path.join(__dirname, 'public')));

http.createServer(nestub).listen(nestub.get('port'), nestub.get('host'), function () {
    debug('Express server listening on port %d in %s mode', nestub.get('port'), nestub.settings.env);
});