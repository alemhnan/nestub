/*jshint node: true, -W032*/
'use strict';

var debug = require('debug')('nestub:neerror');
var util  = require('util');

var NEError = function (err, originalError, infos, notify, constr) {
    Error.captureStackTrace(this, constr || this);
    this.message = err.message || 'Error';
    this.code    = err.code;
    this.icode   = err.icode;
    this.http    = err.http;
    this.notify  = notify || err.notify;
    this.originalError = originalError;
    this.infos = infos;
    debug('\nerr: %j - \noriginalError:%j - \ninfos:%j', err, originalError, infos);
};

util.inherits(NEError, Error);
NEError.prototype.name = 'NE Error';

module.exports = {
    NEError: NEError
};