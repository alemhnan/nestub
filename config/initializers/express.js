/*jshint node: true, -W032, -W106*/
'use strict';

/**
 * Module dependencies.
 */

var express          = require('express');
var path             = require('path');
var neErrorHandler   = require('../../app/middlewares/errors');
var debug            = require('debug')('NEStub:express');

module.exports = function () {

    var app = express();

    app.disable('x-powered-by');
    app.set('port', process.env.PORT || 3000);

    // Middlewares

    if ('development' === app.get('env')) {
        app.set('showStackError', true);
        app.use(express.logger('dev'));
        app.locals.pretty = true;
    };


    app.use(express.compress());
    app.use(express.query());
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    app.use(express.cookieParser());
    
    app.use(app.router);

    app.use(express.favicon(path.join(__dirname, '../../public/images/favicon.ico')));

    app.use(neErrorHandler);
    app.use(express.errorHandler());

    app.all('*', function (req, res, next) {

        res.header('Server', process.env.SERVER_NAME || 'CoffeeStrap');
        next();
    });
    
    debug('app created');
    return app;

};