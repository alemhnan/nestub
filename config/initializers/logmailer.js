/*jshint node: true, -W032, -W106*/
'use strict';

var sendMailer = require('./nemailer').sendMail;

var _error = function(subject, body){
    debugger;
    //console.log('ERROR:' + subject + ' '+ body);
    sendMailer(
        process.env.SMTP_USERNAME,
        process.env.LOG_TO_MAIL,
        '[NEStub API ERROR]  ' + subject,
         body);
};

var _info = function(subject, body){
    //console.log('INFO:' + subject + ' '+ body);
    sendMailer(
        process.env.SMTP_USERNAME,
        process.env.LOG_TO_MAIL,
        '[NEStub API INFO]  ' + subject,
        body);
};

var _log = function(subject, body){
    console.log('LOG:' + subject + ' '+ body);
    //console.log(subject + ' '+ body);
};

var _empty = function(){};

var logger = {};

if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
    logger.error = _error;
    logger.info  = _info;
    logger.log   = _log;

} else if (process.env.NODE_ENV === 'test'){
    //does not work in test, I guess because is async without callback
    // logger.error = _error;
    // logger.info  = _info;
    // logger.log   = _log;

    // logger.error = _log;
    // logger.info  = _log;
    // logger.log   = _log;

    logger.error = _empty;
    logger.info  = _empty;
    logger.log   = _empty;


} else if (process.env.NODE_ENV === 'development') {

    logger.error = _error;
    logger.info  = _info;
    logger.log   = _log;

    // logger.error = _log;
    // logger.info  = _log;
    // logger.log   = _log;
};



module.exports = logger;