/*jshint node: true, -W032, -W106*/
'use strict';

var mongoose = require('mongoose');
var Q        = require('q');
var debug    = require('debug')('nestub:mongoose');


//https://gist.github.com/jasoncrawford/5873309 
// Workaround for the fact that chai-as-promised isn't working with Mongoose promises:
// https://github.com/domenic/chai-as-promised/issues/30
mongoose.Query.prototype.qexec = function () {
    return Q.npost(this, 'exec', arguments);
};

mongoose.Model.qfindById = function(){
    return Q.npost(this, 'findById', arguments);
//    this.model(this.constructor.modelName).findById({ _id: id }).qexec();
};

mongoose.Model.qcreate = function () {
    return Q.npost(this, 'create', arguments);
};

// mongoose.Model.qupdate = function () {
//   return Q.npost(this, 'update', arguments);
// };

mongoose.Model.prototype.qsave = function () {
    var deferred = Q.defer();
    this.save(function (err, model) {
        if (err) {
            deferred.reject(err);
        } else {
            debug('saved!!');
            deferred.resolve(model);
        }
    });
    return deferred.promise;
};

mongoose.Model.prototype.qreload = function () {
    return this.model(this.constructor.modelName).findById(this.id).qexec();
};


var isTrue = process.env.MONGODB_DEBUG === 'true';
mongoose.set('debug', isTrue);


module.exports = function () {

    // Database connect
    var uristring = process.env.MONGODB_URI;
    var mongoOptions = {
        db: {
            safe: true
        }
    };
    debug(uristring);

    var conn = mongoose;
    mongoose.connect(uristring, mongoOptions, function (err, res) {
        if (err) {
            debug('ERROR connecting to: ' + uristring + '. ' + err);
        } else {
            debug('Successfully connected to:%j - %j', uristring, res);
        }
    });

    mongoose.connection.on('error', function (data) {
        debug('oh oh: ' + data);
    });

    // mongoose.connection.once('open', function(data){
    //   debug('Successfully connected to: ' + uristring);
    // });
    return conn;
};