/*jshint node: true, -W032, -W106*/
'use strict';

var nodemailer = require('nodemailer');
var Q          = require('q');
var debug      = require('debug')('nestub:nodemailer');
var util       = require('util');

var smtpTransport = nodemailer.createTransport('SMTP',{
    service: 'Gmail',
    auth: {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD
    }
});

var smptTransportSendMailQ = Q.denodeify(smtpTransport.sendMail);


var sendMail = function(_from, _to, _subject, _textBody){
    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: _from, // sender address
        to: _to, // list of receivers
        subject: _subject, // Subject line
        text: _textBody // plaintext body
    };

    // send mail with defined transport object
    return smptTransportSendMailQ(mailOptions)
    .then(function(res){
        debug('Mail sent:%j', JSON.stringify(res));
    })
    .fail(function(err){
        var errInspect = util.inspect(err, { showHidden: true, depth: null, color: true});
        console.log(errInspect);
        debug('NODEMAILER ERROR, MAIL NOT SENT:%s', errInspect);
    });

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages

};


exports.sendMail = sendMail;