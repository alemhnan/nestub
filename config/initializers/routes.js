/*jshint node: true, -W032*/
'use strict';

var utils = require('../../app/controllers/utilsController');

var prefix = '';

module.exports = function (app) {

    app.get(prefix + '/allroutes', function(req, res){
        var util = require('util');
        // app.routes['get']
        console.log(util.inspect(app.routes, { showHidden: true, depth: null, colors: true }));
        return res.send(200, app.routes);
    });

    //ping
    app.get(prefix + '/ping', utils.ping);

    app.get(prefix + '/errorA', utils.errA);
    app.get(prefix + '/errorB', utils.errB);
    app.get(prefix + '/errorC', utils.errC);
    app.get(prefix + '/errorD', utils.errD);

};