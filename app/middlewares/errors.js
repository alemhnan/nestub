/*jshint node: true, -W032, -W106*/
'use strict';

var util    = require('util');
var logger  = require('../../config/initializers/logmailer.js');
var debug   = require('debug')('nestub:errors');
var NEError = require('../../libs/NEError').NEError;

var catchNEError = function (err, req, res, next) {

    var fullURL = req.protocol + '://' + req.get('host') + req.url;
    var mailOutputStr =
    '\n' +
    'REQUEST: %s\n\n' +
    'CODE: %s\n\n' +
    'ICODE: %s\n\n' +
    'MESSAGE: %s\n\n' +
    'INFOS: %j\n\n' +
    'ORIGINAL_ERROR: %j\n\n' +
    'ERR OBJECT: %j\n\n' +
    'STACK: %s\n\n';// +

    var mailOutput = util.format(
        mailOutputStr,
        fullURL,
        err.code,
        err.icode,
        err.message,
        err.infos,
        err.originalError,
        err,
        err.stack);


    debug('%s', mailOutput);

    if(err.notify) {
        logger.error(err.code, mailOutput);
    };

    var resOutput = {
        error: {
            code: err.code,
            message: err.message,
            icode: err.icode,
            //err: err.originalError,
            infos: err.infos
        }
    };

    return res.send(err.http, resOutput);

};



module.exports = function (err, req, res, next) {
    if (!err) {
        return next();
    }
    debug('%j', err.message);

    if (true === err instanceof NEError) {
        return catchNEError(err, req, res, next);
    } else {
        logger.error('Surprise!', err);
        debug('SURPRISE:%j', err);
        next(err);
    };

};


Object.defineProperty(global, '__stack', {
    get: function () {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function (_, stack) {
            return stack;
        };
        var err = new Error();
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
    get: function () {
        return __stack[1].getLineNumber();
    }
});