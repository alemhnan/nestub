/*jshint node: true, -W032*/
'use strict';

var _ = require('underscore');
var debug = require('debug')('nestub:errorList');

debug('start error list creation');
debugger;

var errorArray = [{
    notify: true,
    icode: -1,
    code: '000042',
    http: 500,
    message: 'Work in progress!!'
},  {
    notify: true,
    icode: 0,
    code: '000000',
    http: 600,
    message: 'Server error! Help!'
},  {
    notify: false,
    icode: 1,
    code: '404001',
    http: 404,
    message: 'Something not found!'
}];

var _errorList = {};
var _errorIList = {};

var errorArrayLength = errorArray.length;
var error = null;
for (var i = 0; i < errorArrayLength; i++) {
    error = errorArray[i];

    if (_.has(_errorList, error.code)) {
        console.log('code - Duplicate in error list! WHAT THE HELL IS GOING ON HERE!!!?');
        debug('code - Duplicate in error list! WHAT THE HELL IS GOING ON HERE!!!?');
        process.exit(1);
    };

    if (_.has(_errorIList, error.icode)) {
        console.log('icode - Duplicate in error list! WHAT THE HELL IS GOING ON HERE!!!?');
        debug('icode - Duplicate in error list! WHAT THE HELL IS GOING ON HERE!!!?');
        process.exit(1);
    };

    if (undefined === error.code || undefined === error.icode || undefined === error.http || undefined === error.message) {
        console.log('Come back here buddy and check if you forgot something');
        debug('Come back here buddy and check if you forgot something');
        process.exit(1);
    };

    _errorList[error.code] = error;
    _errorIList[error.icode] = error;

}



exports.errorList = _errorList;
exports.errorIList = _errorIList;