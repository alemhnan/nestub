/*jshint node: true, -W032*/
'use strict';


var debug = require('debug')('NEStub:utilsController');

var NEError   = require('../../libs/NEError').NEError;
var errorList = require('../middlewares/errorList').errorList;

exports.ping = function (req, res) {
    debug('pong!');
    return res.send(200, {data: 'pong'});
};


exports.errA = function (req, res, next) {
    return next(new NEError(errorList['000042']));
};

exports.errB = function (req, res, next) {
    var originalErr = new Error('Someerror');
    return next(new NEError(errorList['000042'], originalErr));
};

exports.errC = function (req, res, next) {
    var originalErr = new Error('Someerror');
    var infos = {
        infoA: 'some info',
        infoB: 'some other info'
    };
    return next(new NEError(errorList['000042'], originalErr, infos));
};

exports.errD = function (req, res, next) {
    var originalErr = new Error('Someerror');
    var infos = {
        infoA: 'some info',
        infoB: 'some other info'
    };
    var notify = true;//it will override the original notify error parameter
    return next(new NEError(errorList['404001'], originalErr, infos, notify));
};
