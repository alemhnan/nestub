/*jshint node: true, -W032, -W106 */
'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;
var uuid     = require('node-uuid');

module.exports = function () {

    var _schema = new Schema({

        token: {
            type: String,
            required: true,
            unique: true
        },

        createdAt: {
            type: Date,
            required: true,
            'default': Date.now,
            expires: '4h'
        }

    }, {
        strict: true
    });

    _schema.statics.createToken = function () {
        var token = uuid.v4(); //
        return this.qcreate({
            token: token
        });
    };

    mongoose.model('Token', _schema);
};