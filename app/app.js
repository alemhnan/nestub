/*jshint node: true, -W032, -W106 */
'use strict';

module.exports = function () {

    // if (process.env.NODE_ENV === 'production') {
    // }

    // if (process.env.NODE_ENV === 'staging') {
    // }

    //better to require this early on, so to stop if we have errors in the list (i.e. duplicates)
    require('../app/middlewares/errorList');


    var debug = require('debug')('NEStub:app');
    debug('debug:%s', process.env.DEBUG);

    // Bootstrap db connection
    var mongoInit = require('../config/initializers/mongoose.js');
    mongoInit();

    // Bootstrap models
    var tokenModelInit = require('../app/models/Token');
    tokenModelInit();


    // express settings
    var expressInit = require('../config/initializers/express');
    var app = expressInit();

    // Bootstrap routes
    require('../config/initializers/routes')(app);

    return app;
};



// Object.defineProperty(global, '__stack', {
//   get: function(){
//     var orig = Error.prepareStackTrace;
//     Error.prepareStackTrace = function(_, stack){ return stack; };
//     var err = new Error;
//     Error.captureStackTrace(err, arguments.callee);
//     var stack = err.stack;
//     Error.prepareStackTrace = orig;
//     return stack;
//   }
// });
// Object.defineProperty(global, '__line', {
//   get: function(){
//     return __stack[1].getLineNumber();
//   }
// });